<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEduInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edu_institutions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('city')->nullable();
            $table->string('credentials')->unique();
            $table->tinyInteger('type')->default(\App\EduInstitution::TYPE['SCHOOL']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edu_institutions');
    }
}
