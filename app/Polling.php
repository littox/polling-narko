<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polling extends Model
{
    protected $guarded = ['id'];
    protected $dates = [
        'start_at',
    ];

    public function questions()
    {
        return $this->belongsToMany(Question::class, 'polling_results')->withPivot('answer_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
    public function eduInstitution()
    {
        return $this->belongsTo(EduInstitution::class);
    }

    public function results()
    {
        return $this->hasMany(PollingResult::class);
    }

    public function getTimeAttribute()
    {
        return floor($this->created_at->diffInSeconds($this->start_at)/60) . ':' . $this->created_at->diffInSeconds($this->start_at) % 60;
    }
}
