<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    const OTHER_ANSWER_ID = 1;
    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }
}
