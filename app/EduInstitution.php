<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EduInstitution extends Model
{
    const TYPE = ['OTHER' => 1, 'SCHOOL' => 2];

    public function groups()
    {
        return $this->hasMany(Group::class)->where('active', true);
    }
}
