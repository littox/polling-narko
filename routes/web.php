<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PollingController@index')->name('index');
Route::post('/check-credentials', 'PollingController@checkCredentials')->name('credentials.check');
Route::get('/profession', 'PollingController@profession')->name('index.profession');
Route::post('/to-polling', 'PollingController@toPolling')->name('polling.init');
Route::get('/polling', 'PollingController@polling')->name('polling.index');
Route::post('/postAnswer', 'PollingController@postAnswer')->name('polling.answer.post');
Route::post('/', 'PollingController@postPolling')->name('polling.post');
Route::post('/additionalPolling', 'PollingController@postAdditionalPolling')->name('addpolling.post');
Route::get('/flush', 'PollingController@flushSession')->name('session.delete');
Route::get('/polling-results', 'PollingController@resultIndex')->name('polling.result.index');
Route::post('/polling-results', 'PollingController@getResults')->name('polling.result.get');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
