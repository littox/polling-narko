<?php

namespace App\Http\Controllers;

use App\Answer;
use App\EduInstitution;
use App\Group;
use App\Polling;
use App\Question;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PollingController extends Controller
{

    public function checkCredentials(Request $request)
    {
        $institution = EduInstitution::where('credentials', $request->credentials)->first();
        if (is_null($institution)) {
            return response()->json(['error' => 'Не найдено образовательное учреждение'], 404);
        }
        session()->put('institution', $institution);
        return response()->json([], 200);
    }

    public function toPolling(Request $request)
    {
        if ($request->profession === 'teacher') {
            $institution = session()->get('institution');
            $group = Group::firstOrCreate([
                'name' => $request->group,
                'edu_institution_id' => $institution->id,

            ], ['students_count' => $request->students_count]);
            if ($group->active) {
                return response()->json(['error' => 'По указанной группе опрос уже проходил'], 403);
            }
            session()->put('group', $group);
            session()->put('question_type', Question::TYPE['TEACHER']);
            return response()->json([], 200);
        }

        $group = Group::find($request->student_group);
        if (is_null($group)) {
            return response()->json(['error' => 'Не найдено такой группы'], 404);
        }
        session()->put('group', $group);
        session()->put('question_type', Question::TYPE['STUDENT']);
        return response()->json([], 200);
    }


    public function index(Request $request)
    {

//        $newPolling = session()->has('polling');

        if (session()->has('institution')) {
            return response()->redirectTo(route('index.profession'));
        }
        return view('index');
    }

    public function profession(Request $request)
    {

//        $newPolling = session()->has('polling');
        if (session()->has('profession')) {
            return response()->redirectTo(route('polling.index'));
        }
        if (!session()->has('institution')) {
            return response()->redirectTo(route('index'));
        }
        $institution = session()->get('institution');
        $institution->load('groups');
        return view('profession', compact('institution'));
    }

    public function polling(Request $request)
    {
        if (!session()->has('question_type')) {
            return response()->redirectTo(route('index'));
        }

//        $questionType = Question::TYPE['ADDITIONAL'];
        $questionType = session()->get('question_type', Question::TYPE['STUDENT']);
        $questions = Question::with(['answers' => function ($query) {
            $query->orderBy('answer_order', 'asc');
        }])->where('type', $questionType)->orderBy('created_at', 'asc')->get()->toTree()->toJson();

        $newPolling = true;
        if (session()->has('polling')) {
            $questionsWithAnswers = collect(session('polling'))->toJson();
            $newPolling = false;
        } else {
            $questionsWithAnswers = Question::select('questions.id', 'questions.answersCount')
                ->has('answers')
                ->where('type', $questionType)
                ->get()
                ->keyBy('id')
                ->map(function ($item, $key) {
                    if ($item->answersCount > 1) {
                        return [];
                    }
                    return 0;
                })
                ->toJson();
        }
//        $otherAnswerId = Answer::OTHER_ANSWER_ID;

        $questionsWithOther = collect(['o' => 'others'])->toJson();
        if (session()->has('others')) {
            $questionsWithOther = collect(session('others'))->toJson();
        }

        session()->put('start_at', now()->toDateTimeString());

        return view('home', compact('questions',
                'questionsWithAnswers',
//                'otherAnswerId',
                'questionsWithOther',
                'newPolling',
                'questionType')
        );
    }

    public function postAnswer(Request $request)
    {
        session()->put('polling', $request->polling);
        session()->put('others', $request->others);
    }

    public function postPolling(Request $request)
    {
        if (in_array([], $request->polling) || in_array(0, $request->polling)) {
            return response()->json(['error' => 'Вы ответили не на все вопросы!'], 422);
        }

        $group = session()->get('group');
        $polling = Polling::create([
            'edu_institution_id' => $group->edu_institution_id,
            'group_id' => $group->id,
            'start_at' => session()->get('start_at'),
        ]);

        foreach ($request->polling as $question => $answers) {
            foreach ((array)$answers as $answer) {
                $pivot = ['answer_id' => $answer];
                if (!empty($request->others[$question])) {
                    $pivot['other_text'] = $request->others[$question];
                }
                $polling->questions()->attach($question, $pivot);
            };
        }

        if ($request->question_type == Question::TYPE['TEACHER']) {
            $group->active = true;
            $group->save();
        }

        session()->flush();

        if ($request->question_type == Question::TYPE['STUDENT']) {
            session()->put('polling_model', $polling);
            session()->put('question_type', Question::TYPE['ADDITIONAL']);
        }

        return response()->json([], 200);
    }

    public function postAdditionalPolling(Request $request)
    {
        if (in_array([], $request->polling) || in_array(0, $request->polling)) {
            return response()->json(['error' => 'Вы ответили не на все вопросы!'], 422);
        }

        $polling = session()->get('polling_model');

        foreach ($request->polling as $question => $answers) {
            foreach ((array)$answers as $answer) {
                $pivot = ['answer_id' => $answer];
                if (!empty($request->others[$question])) {
                    $pivot['other_text'] = $request->others[$question];
                }
                $polling->questions()->attach($question, $pivot);
            };
        }

        session()->flush();

        return response()->json([], 200);
    }

    public function flushSession(Request $request)
    {
        session()->flush();
        return redirect('/');
    }

    public function resultIndex(Request $request)
    {
        return view('result-index');
    }

    public function getResults(Request $request)
    {
        if (\Auth::guest()) {
            return redirect('/');
        }

        $date = new Carbon($request->date);
        $filePath = storage_path('app/res-' . $request->date . '.csv');

        $file = fopen($filePath, 'w');

        $questions = Question::all()->keyBy('id');
        $columns = array_merge(
            [
                'ИД',
                'Дата прохождения опроса',
                'Название УЗ',
                'Класс\Группа',
                'Время выполнения',
                'Нас. пункт',
                'Кол-во студентов в группе',
            ],
            $questions->pluck('id')->toArray()
        );

        fputcsv($file, $columns, ';');

        Polling::whereDate('created_at', $date->toDateString())
            ->with('results.answer', 'group', 'eduInstitution')
            ->chunk(100, function ($pollings) use ($questions, $file) {
                $pollings->map(function ($item, $key) use($questions, $file){
                    $keyedAnswers = $item->results->groupBy('question_id');
                    $answers = $questions->map(function ($item, $key) use ($keyedAnswers){
                        if ($keyedAnswers->has($item->id)) {
                            return $keyedAnswers[$item->id]->implode('answer.text', ', ') . '-' . $keyedAnswers[$item->id]->implode('other_text', ', ');
                        }
                        return 'null';
                    });
                    $pData = [
                        $item->id,
                        $item->created_at->toDateTimeString(),
                        $item->eduInstitution ? $item->eduInstitution->name : 'null',
                        $item->group ? $item->group->name : 'null',
                        $item->time,
                        $item->eduInstitution ? $item->eduInstitution->city : 'null',
                        $item->group ? $item->group->students_count : 'null',
                    ];
                    fputcsv($file, array_merge($pData, $answers->toArray()), ';');
                });
        });

        fclose($file);

        $filePathCp1251 = storage_path('app/res-' . $request->date . '-cp1251.csv');

        file_put_contents($filePathCp1251, iconv('UTF-8', 'cp1251//TRANSLIT', file_get_contents($filePath)));
//
        return response()->download($filePathCp1251);
    }
}
