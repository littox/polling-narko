<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePollingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pollings', function (Blueprint $table) {
            $table->unsignedInteger('group_id')->nullable();
            $table->unsignedInteger('edu_institution_id')->nullable();
            $table->dateTime('start_at')->nullable();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('edu_institution_id')->references('id')->on('edu_institutions')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pollings', function (Blueprint $table) {
            $table->dropColumn(['group_id', 'edu_institution_id', 'start_at']);
        });
    }
}
