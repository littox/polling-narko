<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Question extends Model
{
    use NodeTrait;

    const TYPE = ['TEACHER' => 1, 'STUDENT' => 2, 'ADDITIONAL' => 3];

    public function answers()
    {
        return $this->belongsToMany(Answer::class)->withPivot('answer_order');
    }
    public function question()
    {
        return $this->belongsTo(Question::class, 'parent_id');
    }
    public function parentId()
    {
        return $this->belongsTo(Question::class, 'parent_id', 'id');
    }
}
