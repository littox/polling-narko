@extends('layouts.app')
{{--TYPE = ['TEACHER' => 1, 'STUDENT' => 2, 'ADDITIONAL' => 3];--}}
@section('content')
    @if($questionType === 1)
        <teacher-questions :questions="{{$questions}}"
                   :questions-with-answers="{{$questionsWithAnswers}}"
                   :questions-with-other="{{$questionsWithOther}}"
                   :new-polling="{{ $newPolling ? 'true' : 'false' }}"></teacher-questions>
    @endif
    @if($questionType === 2)
        <questions :questions="{{$questions}}"
                   :questions-with-answers="{{$questionsWithAnswers}}"
                   :questions-with-other="{{$questionsWithOther}}"
                   :new-polling="{{ $newPolling ? 'true' : 'false' }}"></questions>
    @endif
    @if($questionType === 3)
        <additional-questions :questions="{{$questions}}"
                   :questions-with-answers="{{$questionsWithAnswers}}"
                   :questions-with-other="{{$questionsWithOther}}"
                   :new-polling="{{ $newPolling ? 'true' : 'false' }}"></additional-questions>
    @endif
    {{--{{dd($questions)}}--}}
@endsection
