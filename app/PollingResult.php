<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PollingResult extends Model
{
    protected $guarded = ['id'];
//    protected $table = 'polling_results';

//    public function questions()
//    {
//        return $this->belongsToMany(Question::class, 'polling_results')->withPivot('answer_id');
//    }
    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }
}
